import * as React from 'react';
// @ts-ignore
import { Detector } from 'react-detect-offline';
import {connect} from 'react-redux'
import {Dispatch} from "redux";
import {HasOfflineState, notifyOffline, notifyOnline} from "./offline";

interface DispatchProps {
    goOnline: () => void,
    goOffline: () => void
}

export type Props = DispatchProps;

/**
 * A component that renders only as a div but will fire OfflineNotificationActions when the browser goes offline
 * and online.
 * @param goOffline The function to execute when the browser goes offline.
 * @param goOnline The function to execute when the browser goes online.
 * @constructor
 */
const OfflineSupportDetectionComponent = ({goOffline, goOnline}: Props) => {
    interface SensorProps {
        online: boolean
    }
    const sensor = ({online}: SensorProps) => {
        if (online) {
            goOnline()
        }
        else {
            goOffline();
        }
        return (
            <div>
            </div>)
    };
    return (<Detector render={sensor}/>
    )
};

const mapStateToProps: (s: HasOfflineState) => {} = () => {
    return {
    }
};

const mapDispatchToProps: (d: Dispatch) => DispatchProps = dispatch => {
    return {
        goOnline: () => {
            dispatch(notifyOnline());
        },
        goOffline: () => {
            dispatch(notifyOffline());
        }
    }
};

/**
 * A component that renders only as a div but will fire OfflineNotificationActions when the browser goes offline
 * and online.
 */
export const OfflineSupportDetection = connect(
    mapStateToProps,
    mapDispatchToProps
)(OfflineSupportDetectionComponent);
