import ReactDOM from 'react-dom';
import { Epic, EpicMiddleware } from 'redux-observable';
import { Action, Store } from 'redux';
import { ReactElement } from 'react';
import { elvis } from './elvis';

/**
 * Props that can be injected into a react element that will hide a loading screen.
 */
export interface DisplayInitialiserProps {
  readonly onDisplayInitialised: () => void;
}

/**
 * A function to create a react app that may have a loading screen that needs to be removed.
 * @param store The react store to use.
 * @param epicMiddleware The epic middleware to use.
 * @param rootElementId The id of the element where the react app will be rendered.
 * @param loadingElementId The id of the element that contains the loading screen.
 * @param renderer The react element to render that may have InitialiserDisplayProps.
 * @param epicBuilder A builder to create an epic given the root element.
 * @param actions The actions to run by the store dispatcher.
 */
export function initialiseDisplay<T extends Action, O extends T = T, S = void, D = any>(
  store: Store,
  epicMiddleware: EpicMiddleware<T, O, S, D>,
  rootElementId: string,
  loadingElementId: string | undefined,
  renderer: (initialiser: () => void) => ReactElement,
  epicBuilder: (root: HTMLElement) => Epic,
  ...actions: Array<Action<string>>
): void {
  const root = document.getElementById(rootElementId) || undefined;
  const loading = elvis(loadingElementId, el => document.getElementById(el));

  const eventName = 'RROOS:INITIALISED';
  if (root) {
    if (loading) {
      loading.addEventListener(eventName, () => {
        const parent = loading.parentNode;
        if (parent) {
          parent.removeChild(loading);
        }
      });
    }
    const initialiser = () => {
      if (loading) {
        loading.dispatchEvent(new Event(eventName));
      }
    };
    ReactDOM.render(renderer(initialiser), root);
    epicMiddleware.run(epicBuilder(root));
    actions.forEach(action => store.dispatch(action));
  }
}
