import { Action, Reducer } from 'redux';
import { Seq } from 'immutable';
import { EMPTY, of, OperatorFunction } from 'rxjs';
import { flatMap } from 'rxjs/operators';

/**
 * A function that can be used to build another function that then check to see if an action has a given type
 * and return it if it does or return undefined if it does not.
 * @param type The value of type to check for.
 */
export function actionExtractor<T extends string, A extends Action<T>>(type: T): (action: Action<T>) => A | undefined {
  return action => {
    if (action.type === type) {
      return action as A;
    } else {
      return undefined;
    }
  };
}

/**
 * An enhancement of ofType that uses an action extractor to check against the actions' type as well as using the
 * extractor's type information that to also narrow the type of an Observable.
 * @param extractor
 */
export function listenFor<T extends string, A extends Action<T>>(
  extractor: (action: Action<T>) => A | undefined,
): OperatorFunction<Action<T>, A> {
  return action$ =>
    action$.pipe(
      flatMap(action => {
        const maybeAction = extractor(action);
        return maybeAction ? of(maybeAction) : EMPTY;
      }),
    );
}

/**
 * A class used to generate type-safe, switch free reducers.
 */
export class ReducerBuilder<S> {
  private readonly defaultState: S;
  private readonly reducers: Seq.Indexed<(s: S, a: Action<string>) => S>;

  public constructor(defaultState: S, reducers: Seq.Indexed<(s: S, a: Action<string>) => S> = Seq.Indexed()) {
    this.defaultState = defaultState;
    this.reducers = reducers;
  }

  /**
   * Add a case for this reducer builder to consider.
   * @param predicate A function that takes an action and returns the action if it matches or undefined if it does not,
   * @param stageChanger A function that will change state of the action matched the predicate.
   */
  public when<T extends string, A extends Action<T>>(
    predicate: (a: Action<T>) => A | undefined,
    stageChanger: (a: A) => (s: S) => S,
  ) {
    const reducer: (s: S, a: Action<string>) => S = (s, a) => {
      const maybeAction = predicate(a as Action<T>);
      return maybeAction ? stageChanger(maybeAction)(s) : s;
    };
    return new ReducerBuilder<S>(this.defaultState, this.reducers.concat([reducer]));
  }

  /**
   * Build a reducer that considers all cases added by when()
   */
  public build(): Reducer<S, Action<string>> {
    return (s, action) => {
      const state = s || this.defaultState;
      if (!action || !action.type) {
        return state;
      }
      return this.reducers.reduce((currentState, reducer) => reducer(currentState, action), state);
    };
  }
}
