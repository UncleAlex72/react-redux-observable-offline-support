/**
 * A simple function that allows another function to be run on a value if it is not undefined.
 * @param value The value to use.
 * @param flatMap A function that will be called if the value is defined.
 */
export function elvis<T, U>(value: T | undefined, flatMap: (t: T) => U | undefined): U | undefined {
  if (value === undefined) {
    return undefined;
  } else {
    return flatMap(value);
  }
}
