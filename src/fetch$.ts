import { concat, merge, MonoTypeOperatorFunction, Observable, of } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { filter } from 'rxjs/internal/operators/filter';
import { flatMap, map } from 'rxjs/operators';

/**
 * An interface that reflects the normal Body interface but all body accessors are observable and suffixed with a $.
 */
export interface Body$ {
  readonly bodyUsed: boolean;
  body$(): Observable<ReadableStream<Uint8Array>>;
  arrayBuffer$(): Observable<ArrayBuffer>;
  blob$(): Observable<Blob>;
  formData$(): Observable<FormData>;
  json$<T = any>(): Observable<T>;
  text$(): Observable<string>;
}

/**
 * An interface that reflects the normal Response interface but all body accessors are observable and suffixed with a $.
 */
export interface Response$ extends Body$ {
  readonly headers: Headers;
  readonly ok: boolean;
  readonly redirected: boolean;
  readonly status: number;
  readonly statusText: string;
  readonly trailer$: Observable<Headers>;
  readonly type: ResponseType;
  readonly url: string;
  clone(): Response$;
}

/**
 * An implementation of @{link Response$} that wraps a normal response.
 */
class ResponseAdapter$ implements Response$ {
  public headers: Headers;
  public ok: boolean;
  public redirected: boolean;
  public status: number;
  public statusText: string;
  public trailer$: Observable<Headers>;
  public type: ResponseType;
  public url: string;
  public bodyUsed: boolean;

  private response: Response;

  constructor(response: Response) {
    this.response = response;
    this.bodyUsed = response.bodyUsed;
    this.headers = response.headers;
    this.ok = response.ok;
    this.redirected = response.redirected;
    this.status = response.status;
    this.statusText = response.statusText;
    this.trailer$ = fromPromise(response.trailer);
    this.type = response.type;
    this.url = response.url;
  }

  public body$(): Observable<ReadableStream<Uint8Array>> {
    const body = this.response.body;
    return body ? of(body) : of();
  }

  public arrayBuffer$(): Observable<ArrayBuffer> {
    this.bodyUsed = true;
    return fromPromise(this.response.arrayBuffer());
  }

  public blob$(): Observable<Blob> {
    this.bodyUsed = true;
    return fromPromise(this.response.blob());
  }

  public formData$(): Observable<FormData> {
    this.bodyUsed = true;
    return fromPromise(this.response.formData());
  }

  public json$<T = any>(): Observable<T> {
    this.bodyUsed = true;
    return fromPromise(this.response.json());
  }

  public text$(): Observable<string> {
    this.bodyUsed = true;
    return fromPromise(this.response.text());
  }

  public clone(): Response$ {
    this.bodyUsed = true;
    return new ResponseAdapter$(this.response.clone());
  }
}

/**
 * A function that filters an observable of Response$
 * @param statusOrPredicate Either a status predicate or a status to allow.
 */
export function ifStatus(
  statusOrPredicate: number | ((status: number) => boolean),
): MonoTypeOperatorFunction<Response$> {
  const statusPredicate =
    typeof statusOrPredicate === 'number' ? (status: number) => statusOrPredicate === status : statusOrPredicate;
  const responsePredicate = (response: Response$) => statusPredicate(response.status);
  return filter(responsePredicate);
}

/**
 * A filter that allows valid HTTP responses with a status of less than 400.
 */
export const ifValid: MonoTypeOperatorFunction<Response$> = ifStatus((status: number) => status < 400);

/**
 * A filter that allows invalid HTTP responses with a status of at least 400.
 */
export const ifInvalid: MonoTypeOperatorFunction<Response$> = ifStatus((status: number) => status >= 400);

/**
 * A filter that allows HTTP responses with status 200.
 */
export const ifOk: MonoTypeOperatorFunction<Response$> = ifStatus(200);

/**
 * A filter that allows HTTP responses with status 201.
 */
export const ifCreated: MonoTypeOperatorFunction<Response$> = ifStatus(201);

/**
 * A filter that allows HTTP responses with status 204.
 */
export const ifNoContent: MonoTypeOperatorFunction<Response$> = ifStatus(204);

/**
 * A filter that allows HTTP responses with status 401.
 */
export const ifAuthorisationRequired: MonoTypeOperatorFunction<Response$> = ifStatus(401);

/**
 * A filter that allows HTTP responses with status 403.
 */
export const ifForbidden: MonoTypeOperatorFunction<Response$> = ifStatus(403);

/**
 * A filter that allows HTTP responses with status 404.
 */
export const ifNotFound: MonoTypeOperatorFunction<Response$> = ifStatus(404);

/**
 * A function that throws the response as an error if the HTTP status code is 400 or more.
 */
export function errorIfNotValid(): MonoTypeOperatorFunction<Response$> {
  return (response$: Observable<Response$>) => {
    const valid$ = response$.pipe(ifValid);
    const invalid$ = response$.pipe(
      ifInvalid,
      flatMap(invalidResponse => {
        throw invalidResponse;
      }),
    );
    return merge(valid$, invalid$);
  };
}

export function defaultCredentialsTo(defaultCredentials: RequestCredentials): (init?: RequestInit) => RequestInit {
  const credentials: RequestInit = { credentials: defaultCredentials };
  return (init: RequestInit | undefined) => {
    if (init === undefined || init.credentials === undefined) {
      return Object.assign(credentials, init);
    } else {
      return init;
    }
  };
}
/**
 * A wrapper around fetch that returns an observable Response$ instead of a Response promise. The only other difference
 * is that credentials default to 'same-origin'.
 * @param input The request info.
 * @param init The request initializer.
 */
export function fetch$(input: RequestInfo, init?: RequestInit): Observable<Response$> {
  const initWithCredentials = defaultCredentialsTo('same-origin')(init);
  return fromPromise(fetch(input, initWithCredentials)).pipe(map(response => new ResponseAdapter$(response)));
}
