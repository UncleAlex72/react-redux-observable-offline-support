import {
    dismissRequestPushNotificationsAlert, dismissUpdatedContentAvailableAlert,
    serviceWorkerAlerts,
    showRequestPushNotificationsAlert, showUpdatedContentAvailableAlert
} from "../src";

test("The default state for the service worker alerts reducer should be to show nothing", () => {
    // @ts-ignore
    expect(serviceWorkerAlerts(undefined, {})).toEqual({
        showNotificationsAlert: false,
        showNewContentAlert: false
    })
});

test("The show notifications alert action " +
    "should set showing notifications to true and not change showing updated content.", () => {
    const state = {
        showNotificationsAlert: false,
        showNewContentAlert: false
    };
    expect(serviceWorkerAlerts(state, showRequestPushNotificationsAlert())).toEqual({
        showNotificationsAlert: true,
        showNewContentAlert: false
    })
});

test("The dismiss notifications alert action " +
    "should set showing notifications to false and not change showing updated content.", () => {
    const state = {
        showNotificationsAlert: true,
        showNewContentAlert: true
    };
    expect(serviceWorkerAlerts(state, dismissRequestPushNotificationsAlert())).toEqual({
        showNotificationsAlert: false,
        showNewContentAlert: true
    })
});

test("The show updated content alert action " +
    "should set showing updated content to true and not change showing notification alerts.", () => {
    const state = {
        showNotificationsAlert: false,
        showNewContentAlert: false
    };
    expect(serviceWorkerAlerts(state, showUpdatedContentAvailableAlert())).toEqual({
        showNotificationsAlert: false,
        showNewContentAlert: true
    })
});

test("The dismiss updated content alert action " +
    "should set showing updated content to false and not change showing notification alerts.", () => {
    const state = {
        showNotificationsAlert: true,
        showNewContentAlert: true
    };
    expect(serviceWorkerAlerts(state, dismissUpdatedContentAvailableAlert())).toEqual({
        showNotificationsAlert: true,
        showNewContentAlert: false
    })
});
