import { Action, Reducer } from 'redux';
import { actionExtractor, ReducerBuilder } from './reducers';

/**
 * An action used to notify that the browser is online.
 */
export interface NotifyOnlineAction extends Action<string> {
  type: 'NOTIFY_ONLINE';
}

/**
 * An action used to notify that the browser is offline.
 */
export interface NotifyOfflineAction extends Action<string> {
  type: 'NOTIFY_OFFLINE';
}

/**
 * The superclass of offline notification actions.
 */
export type OfflineNotificationAction = NotifyOnlineAction | NotifyOfflineAction;

/**
 * Create a NotifyOnlineAction.
 */
export function notifyOnline(): NotifyOnlineAction {
  return {
    type: 'NOTIFY_ONLINE',
  };
}

const isNotifyOnlineAction = actionExtractor<'NOTIFY_ONLINE', NotifyOnlineAction>('NOTIFY_ONLINE');

/**
 * Create a NotifyOfflineAction.
 */
export function notifyOffline(): NotifyOfflineAction {
  return {
    type: 'NOTIFY_OFFLINE',
  };
}

const isNotifyOfflineAction = actionExtractor<'NOTIFY_OFFLINE', NotifyOfflineAction>('NOTIFY_OFFLINE');

/**
 * A state to be exposed to redux that stores whether a browser is online or offline.
 */
export type OfflineState = boolean;

export const offline: Reducer<OfflineState, OfflineNotificationAction> = new ReducerBuilder(false)
  .when(isNotifyOfflineAction, () => () => true)
  .when(isNotifyOnlineAction, () => () => false)
  .build();

/**
 * A marker interface that allows combined state to declare it has offline state.
 */
export interface HasOfflineState {
  offline: OfflineState;
}

/**
 * A state accessor to see if the browser is offline.
 * @param s The overall state of the application.
 */
export const isOffline = (s: HasOfflineState) => s.offline;

/**
 * A state accessor to see if the browser is online.
 * @param s The overall state of the application.
 */
export const isOnline = (s: HasOfflineState) => !s.offline;
