import { EMPTY, Observable } from 'rxjs';
import { Action } from 'redux';
import { Epic } from 'redux-observable';
import { catchError } from 'rxjs/operators';
import { Response$ } from './fetch$';

/**
 * A builder class that can be used to build wrappers around epics used to catch errors in a standard way.
 */
export class ErrorAwareEpicBuilder {
  private readonly authAction$Builder: (() => Observable<Action<string>>) | undefined;
  private readonly errorAction$Builder: ((error: any) => Observable<Action<string>>) | undefined;

  constructor(
    maybeOnAuthenticationRequired?: () => Observable<Action<string>>,
    maybeOnError?: (error: any) => Observable<Action<string>>,
  ) {
    this.authAction$Builder = maybeOnAuthenticationRequired;
    this.errorAction$Builder = maybeOnError;
  }

  /**
   * Create a new builder that will return the given action stream if a user is unauthorised.
   * @param action$Builder The action stream to follow when a user is unauthorised.
   */
  public onAuthenticationRequired(action$Builder: () => Observable<Action<string>>): ErrorAwareEpicBuilder {
    return new ErrorAwareEpicBuilder(action$Builder, this.errorAction$Builder);
  }

  /**
   * Create a new builder that will return the given action stream on a given error.
   * @param error$Builder The action stream to follow when an error occurs.
   */
  public onError(error$Builder: (error: any) => Observable<Action<string>>): ErrorAwareEpicBuilder {
    return new ErrorAwareEpicBuilder(this.authAction$Builder, error$Builder);
  }

  public build(): (epic: Epic) => Epic {
    return epic => (action$, state$, dependencies) =>
      epic(action$, state$, dependencies).pipe(
        catchError((err: any) => {
          if ((err as Response$) && err.status === 401 && this.authAction$Builder) {
            return this.authAction$Builder();
          } else if (this.errorAction$Builder) {
            return this.errorAction$Builder(err);
          } else {
            return EMPTY;
          }
        }),
      );
  }
}
