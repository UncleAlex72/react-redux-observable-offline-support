import {notifyOffline, notifyOnline, offline} from "../src"

test("The default state for the offline reducer should be offline", () => {
    // @ts-ignore
    expect(offline(undefined, {})).toEqual(false)
});

test("The offline action should change the state to be true", () => {
    expect(offline(false, notifyOffline())).toEqual(true)
});

test("The online action should change the state to be false", () => {
    expect(offline(true, notifyOnline())).toEqual(false)
});