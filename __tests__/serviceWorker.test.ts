import {
    serviceWorker,
    storePushNotificationPublicKey,
    storePushNotificationUrl,
    storePushNotificationUsername
} from "../src";

test("The default state for the service worker reducer should be an empty object", () => {
    // @ts-ignore
    expect(serviceWorker(undefined, {})).toEqual({})
});

test("The store public key action should store a public key", () => {
    expect(
        serviceWorker({}, storePushNotificationPublicKey("key"))
    ).toEqual({"publicKey": "key"});
});

test("The store url action should store a url", () => {
    expect(
        serviceWorker({}, storePushNotificationUrl("https://somewhere/near"))
    ).toEqual({"url": "https://somewhere/near"});
});

test("The store username action should store a username", () => {
    expect(
        serviceWorker({}, storePushNotificationUsername("freddie"))
    ).toEqual({"username": "freddie"});
});