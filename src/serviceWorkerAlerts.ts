import { Action, Reducer } from 'redux';
import { actionExtractor, ReducerBuilder } from './reducers';

export interface AlterUpdatedContentAvailableAlertAction extends Action<string> {
  type: 'ALTER_UPDATED_CONTENT_AVAILABLE_ALERT_VISIBILITY';
  visible: boolean;
}
export interface AlterRequestPushNotificationsAlertAction extends Action<string> {
  type: 'ALTER_REQUEST_PUSH_NOTIFICATIONS_ALERT_VISIBILITY';
  visible: boolean;
}

export type ServiceWorkerAlertAction =
  | AlterRequestPushNotificationsAlertAction
  | AlterUpdatedContentAvailableAlertAction;

function alterUpdatedContentAvailableAlert(visible: boolean): () => AlterUpdatedContentAvailableAlertAction {
  return () => {
    return {
      type: 'ALTER_UPDATED_CONTENT_AVAILABLE_ALERT_VISIBILITY',
      visible,
    };
  };
}

export const showUpdatedContentAvailableAlert = alterUpdatedContentAvailableAlert(true);

export const dismissUpdatedContentAvailableAlert = alterUpdatedContentAvailableAlert(false);

const isAlterUpdatedContentAvailableAlertAction = actionExtractor<
  'ALTER_UPDATED_CONTENT_AVAILABLE_ALERT_VISIBILITY',
  AlterUpdatedContentAvailableAlertAction
>('ALTER_UPDATED_CONTENT_AVAILABLE_ALERT_VISIBILITY');

function alterRequestPushNotificationsAlert(visible: boolean): () => AlterRequestPushNotificationsAlertAction {
  return () => {
    return {
      type: 'ALTER_REQUEST_PUSH_NOTIFICATIONS_ALERT_VISIBILITY',
      visible,
    };
  };
}

export const showRequestPushNotificationsAlert = alterRequestPushNotificationsAlert(true);

export const dismissRequestPushNotificationsAlert = alterRequestPushNotificationsAlert(false);

const isRequestPushNotificationsAlertAction = actionExtractor<
  'ALTER_REQUEST_PUSH_NOTIFICATIONS_ALERT_VISIBILITY',
  AlterRequestPushNotificationsAlertAction
>('ALTER_REQUEST_PUSH_NOTIFICATIONS_ALERT_VISIBILITY');

export interface ServiceWorkerAlertsState {
  readonly showNotificationsAlert: boolean;
  readonly showNewContentAlert: boolean;
}

const initialState: ServiceWorkerAlertsState = {
  showNotificationsAlert: false,
  showNewContentAlert: false,
};

export const serviceWorkerAlerts: Reducer<ServiceWorkerAlertsState, ServiceWorkerAlertAction> = new ReducerBuilder(
  initialState,
)
  .when(isAlterUpdatedContentAvailableAlertAction, action => state => {
    return {
      ...state,
      showNewContentAlert: action.visible,
    };
  })
  .when(isRequestPushNotificationsAlertAction, action => state => {
    return {
      ...state,
      showNotificationsAlert: action.visible,
    };
  })
  .build();

export interface HasServiceWorkerAlerts {
  serviceWorkerAlerts: ServiceWorkerAlertsState;
}

const getState = (s: HasServiceWorkerAlerts) => s.serviceWorkerAlerts;

function getFromState<T>(extractor: (state: ServiceWorkerAlertsState) => T): (state: HasServiceWorkerAlerts) => T {
  return state => extractor(getState(state));
}

export const isNotificationAlertVisible = getFromState(state => state.showNotificationsAlert);
export const isNewContentAlertVisible = getFromState(state => state.showNewContentAlert);
