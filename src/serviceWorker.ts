import { combineEpics, Epic, StateObservable } from 'redux-observable';
import { EMPTY, fromEvent, merge, Observable } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { filter, flatMap, map } from 'rxjs/operators';
import { errorIfNotValid, fetch$ } from './fetch$';
import { Action, Reducer } from 'redux';
import { showRequestPushNotificationsAlert, showUpdatedContentAvailableAlert } from './serviceWorkerAlerts';
import { actionExtractor, listenFor, ReducerBuilder } from './reducers';

/**
 * An action that indicates a service worker should be installed.
 */
export interface InstallServiceWorkerAction extends Action<string> {
  type: 'INSTALL_SERVICE_WORKER';
}

/**
 * An action that indicates that the push notification public key should be fetched from a URL.
 */
export interface FetchPushNotificationPublicKeyAction extends Action<string> {
  type: 'FETCH_PUSH_NOTIFICATION_PUBLIC_KEY';
  publicKeyUrl: string;
}

/**
 * An action that indicates that the push notification public key should be stored.
 */
export interface StorePushNotificationPublicKeyAction extends Action<string> {
  type: 'STORE_PUSH_NOTIFICATION_PUBLIC_KEY';
  publicKey: string;
}

/**
 * An action that indicates that the URL for storing push notifications should be stored.
 */
export interface StorePushNotificationUrlAction extends Action<string> {
  type: 'STORE_PUSH_NOTIFICATION_URL';
  url: string;
}

/**
 * An action that indicates that the username to be sent as part of a push notification should be stored..
 */
export interface StorePushNotificationUsernameAction extends Action<string> {
  type: 'STORE_PUSH_NOTIFICATION_USERNAME';
  username: string;
}

/**
 * An action that indicates that the user has agreed to recieve push notifications and so the subscription
 * process should start.
 */
export interface SubscribeToPushNotificationsAction extends Action<string> {
  type: 'SUBSCRIBE_TO_PUSH_NOTIFICATIONS';
}

/**
 * The supertype of all service worker actions.
 */
export type ServiceWorkerAction =
  | InstallServiceWorkerAction
  | FetchPushNotificationPublicKeyAction
  | StorePushNotificationPublicKeyAction
  | StorePushNotificationUrlAction
  | StorePushNotificationUsernameAction
  | SubscribeToPushNotificationsAction;

/**
 * Create an InstallServiceWorkerAction.
 */
export function installServiceWorker(): InstallServiceWorkerAction {
  return {
    type: 'INSTALL_SERVICE_WORKER',
  };
}

const isInstallServiceWorker = actionExtractor<'INSTALL_SERVICE_WORKER', InstallServiceWorkerAction>(
  'INSTALL_SERVICE_WORKER',
);

/**
 * Create a SubscribeToPushNotificationsAction.
 */
export function subscribeToPushNotifications(): SubscribeToPushNotificationsAction {
  return {
    type: 'SUBSCRIBE_TO_PUSH_NOTIFICATIONS',
  };
}

const isSubscribeToPushNotifications = actionExtractor<
  'SUBSCRIBE_TO_PUSH_NOTIFICATIONS',
  SubscribeToPushNotificationsAction
>('SUBSCRIBE_TO_PUSH_NOTIFICATIONS');

/**
 * Create a StorePushNotificationPublicKeyAction.
 * @param publicKey The public key to store.
 */
export function storePushNotificationPublicKey(publicKey: string): StorePushNotificationPublicKeyAction {
  return {
    type: 'STORE_PUSH_NOTIFICATION_PUBLIC_KEY',
    publicKey,
  };
}

const isStorePushNotificationPublicKey = actionExtractor<
  'STORE_PUSH_NOTIFICATION_PUBLIC_KEY',
  StorePushNotificationPublicKeyAction
>('STORE_PUSH_NOTIFICATION_PUBLIC_KEY');

/**
 * Create a StorePushNotificationUrlAction.
 * @param url The URL where push subscriptions are sent.
 */
export function storePushNotificationUrl(url: string): StorePushNotificationUrlAction {
  return {
    type: 'STORE_PUSH_NOTIFICATION_URL',
    url,
  };
}

const isStorePushNotificationUrl = actionExtractor<'STORE_PUSH_NOTIFICATION_URL', StorePushNotificationUrlAction>(
  'STORE_PUSH_NOTIFICATION_URL',
);

/**
 * Create a StorePushNotificationUsernameAction.
 * @param username The username to send when subscribing to push notifications.
 */
export function storePushNotificationUsername(username: string): StorePushNotificationUsernameAction {
  return {
    type: 'STORE_PUSH_NOTIFICATION_USERNAME',
    username,
  };
}

const isStorePushNotificationUsername = actionExtractor<
  'STORE_PUSH_NOTIFICATION_USERNAME',
  StorePushNotificationUsernameAction
>('STORE_PUSH_NOTIFICATION_USERNAME');

/**
 * The Redux state object for push notifications.
 */
export interface PushNotificationState {
  readonly publicKey?: string;
  readonly url?: string;
  readonly username?: string;
}

/**
 * The redux reducer for service workers.
 * @param state The current state.
 * @param action The action used to create a new state.
 */
export const serviceWorker: Reducer<PushNotificationState, ServiceWorkerAction> = new ReducerBuilder({})
  .when(isStorePushNotificationPublicKey, action => state => {
    const publicKey = action.publicKey;
    return {
      ...state,
      publicKey,
    };
  })
  .when(isStorePushNotificationUrl, action => state => {
    const url = action.url;
    return {
      ...state,
      url,
    };
  })
  .when(isStorePushNotificationUsername, action => state => {
    const username = action.username;
    return {
      ...state,
      username,
    };
  })
  .build();

/**
 * An interface that allows redux states mark that they contain a push notification state.
 */
export interface HasServiceWorkerState {
  serviceWorker: PushNotificationState;
}

/**
 * An observable that produces a ServiceWorkerRegistration if it is supported.
 */
function registration$(): Observable<ServiceWorkerRegistration> {
  function validOrigin() {
    const publicUrl = new URL(process.env.PUBLIC_URL || '', window.location.toString());
    return publicUrl.origin === window.location.origin;
  }

  if ('serviceWorker' in navigator && validOrigin()) {
    const path = process.env.PUBLIC_URL;
    const swUrl =
      process.env.NODE_ENV === 'production' ? `${path}/service-worker.js` : `${path}/service-worker-notifications.js`;
    return fromPromise(navigator.serviceWorker.register(swUrl));
  } else {
    return EMPTY;
  }
}

/**
 * Create an observable that, given a service worker registration, gets the push manager and returns it if
 * the user is currently _not_ subscribed.
 * @param registration A service worker registration.
 */
function pushManager$(registration: ServiceWorkerRegistration): Observable<PushManager> {
  const pushManager: PushManager = registration.pushManager;
  const maybeSubscription$ = fromPromise(pushManager.getSubscription());
  return maybeSubscription$.pipe(
    filter(subscription => {
      return subscription === null;
    }),
    map(() => {
      return pushManager;
    }),
  );
}

/**
 * A function that modifies a root element so that it fires a synthetic event when a service worker notifies
 * that it"s state has changed.
 * @param root The root element that will fire the event.
 * @param eventName The name of the event to fire.
 * @param registration A service worker registration.
 */
function checkForUpdates(root: HTMLElement, eventName: string, registration: ServiceWorkerRegistration): void {
  registration.onupdatefound = () => {
    const installingWorker = registration.installing;
    if (installingWorker) {
      installingWorker.onstatechange = () => {
        if (installingWorker.state === 'installed') {
          if (navigator.serviceWorker.controller) {
            const event = new Event(eventName);
            root.dispatchEvent(event);
          } else {
            // Do nothing
          }
        }
      };
    }
  };
}

type PushManager$Builder = (serviceWorkerRegistration: ServiceWorkerRegistration) => Observable<PushManager>;

/**
 * A function that builds epics that listen for an InstallServiceWorkerAction and, depending on the pushManager$Builder,
 * will try and prompt a user if they wish to subscribe to push notifications.
 * @param pushManager$Builder A function that builds a push manager observable from a service worker registration.
 */
const genericInstallServiceWorkerEpicBuilder: (
  pushManager$Builder: PushManager$Builder,
) => (root: HTMLElement, updateEventName: string) => Epic = pushManager$Builder => (root, updateEventName) => action$ =>
  action$.pipe(
    listenFor(isInstallServiceWorker),
    flatMap(registration$),
    flatMap(registration => {
      const requestPushNotifications$ = pushManager$Builder(registration).pipe(map(showRequestPushNotificationsAlert));
      checkForUpdates(root, updateEventName, registration);
      const listenForUpdates$ = fromEvent(root, updateEventName).pipe(map(() => showUpdatedContentAvailableAlert()));
      return merge(requestPushNotifications$, listenForUpdates$);
    }),
  );

/**
 * An epic that installs a service worker and does not attempt to subscribe to any push notifications.
 */
const installServiceWorkerEpic = genericInstallServiceWorkerEpicBuilder(() => EMPTY);

/**
 * An epic that installs a service worker and will also prompt a user to subscribe to push notifications.
 */
const installServiceWorkerWithPushNotificationsEpic = genericInstallServiceWorkerEpicBuilder(pushManager$);

function urlBase64ToUint8Array(base64String: string): Uint8Array {
  const padding = '='.repeat((4 - (base64String.length % 4)) % 4);
  const base64 = (base64String + padding).replace(/-/g, '+').replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

/**
 * Generate an observable of push subscriptions from a push manager and a public key.
 * @param pushManager The browser"s push manager.
 * @param publicKey The application"s public key.
 */
const generateSubscription$ = (pushManager: PushManager, publicKey: string) => {
  const options = {
    userVisibleOnly: true,
    applicationServerKey: urlBase64ToUint8Array(publicKey),
  };
  return fromPromise(pushManager.subscribe(options));
};

/**
 * Store a push subscription on a server.
 * @param username The user"s name, if available.
 * @param url The URL used to store the subscription.
 */
const storeSubscription$ = (username: string | undefined, url: string) => (subscription: PushSubscription) => {
  const payload = Object.assign({ user: { user: username, userAgent: navigator.userAgent } }, subscription.toJSON());
  const body = JSON.stringify(payload);
  return fetch$(url, {
    method: 'post',
    body,
  }).pipe(
    errorIfNotValid(),
    flatMap(() => EMPTY),
  );
};

/**
 * An epic that listens for subscribe to push notification actions and will attempt to subscribe the user to
 * receive push notifications. Note that it is expected that the public key, subscription url and, optionally,
 * the user"s name have all been stored in state previously.
 * @param action$
 * @param state$
 */
const subscribeToPushNotificationsEpic: Epic = (action$, state$: StateObservable<HasServiceWorkerState>) =>
  action$.pipe(
    listenFor(isSubscribeToPushNotifications),
    flatMap(registration$),
    flatMap(pushManager$),
    flatMap(pushManager => {
      const { publicKey, url, username } = state$.value.serviceWorker;
      if (!publicKey) {
        throw new Error('Cannot find a push notification public key.');
      }
      if (!url) {
        throw new Error('Cannot find a subscription url for push notifications.');
      }
      return generateSubscription$(pushManager, publicKey).pipe(flatMap(storeSubscription$(username, url)));
    }),
  );

export const fetchPushNotificationPublicKey$ = (publicKeyUrl: string) => {
  return fetch$(publicKeyUrl).pipe(
    flatMap(response => response.json$()),
    map((json: { publicKey?: string }) => {
      if (json.publicKey) {
        return storePushNotificationPublicKey(json.publicKey);
      } else {
        throw new Error('Cannot find a push notification public key in the public key response.');
      }
    }),
  );
};

export const serviceWorkerWithPushNotificationsEpic = (
  root: HTMLElement,
  updateEventName: string = 'RROOS:CONTENT_UPDATED',
) =>
  combineEpics(installServiceWorkerWithPushNotificationsEpic(root, updateEventName), subscribeToPushNotificationsEpic);

export const serviceWorkerEpic = (root: HTMLElement, updateEventName: string = 'RROOS:CONTENT_UPDATED') =>
  combineEpics(installServiceWorkerEpic(root, updateEventName));
