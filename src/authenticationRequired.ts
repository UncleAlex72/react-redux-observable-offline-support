import { Action, Reducer } from 'redux';
import { actionExtractor, ReducerBuilder } from './reducers';

export type AuthenticationRequiredState = boolean;

export interface RequireAuthenticationAction extends Action<string> {
  type: 'REQUIRE_AUTHENTICATION';
}

export type AuthenticateAction = RequireAuthenticationAction;

export function requireAuthentication(): RequireAuthenticationAction {
  return {
    type: 'REQUIRE_AUTHENTICATION',
  };
}

export const isrequireAuthentication = actionExtractor<'REQUIRE_AUTHENTICATION', RequireAuthenticationAction>(
  'REQUIRE_AUTHENTICATION',
);

export const authenticationRequired: Reducer<AuthenticationRequiredState, AuthenticateAction> = new ReducerBuilder(
  false,
)
  .when(isrequireAuthentication, () => () => {
    return true;
  })
  .build();

interface HasAuthenticationRequired {
  readonly authenticationRequired: AuthenticationRequiredState;
}

export const isAuthenticationRequired: (state: HasAuthenticationRequired) => boolean = state =>
  state.authenticationRequired;
