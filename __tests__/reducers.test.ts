import {Action, Reducer} from "redux";
import {actionExtractor, listenFor, ReducerBuilder} from "../src/reducers";
import {Observable, of} from "rxjs";

interface TestAction extends Action<string> {}

interface ActionA extends TestAction {
    type: "A",
    value: string
}

interface ActionB extends TestAction {
    type: "B",
    value: number
}

function a(value: string): ActionA {
    return {
        type: "A",
        value
    }
}

const isA = actionExtractor<"A", ActionA>("A");

function b(value: number): ActionB {
    return {
        type: "B",
        value
    }
}

const isB = actionExtractor<"B", ActionB>("B");

function c(): Action<string> {
    return {
        type: "WHO_KNOWS?"
    }
}

interface TestState {
    a: string,
    b: number
}

const initialState: TestState = {
    a: "Hello",
    b: 7
};

const reducer: Reducer<TestState, Action<string>> = new ReducerBuilder(initialState)
    .when(isA, action => state => {
        return { ...state, a: action.value }
    })
    .when(isB, action => state => {
        return { ...state, b: action.value }
    })
    .build();

test("The action extractor extracts the action for actions it does know about", () => {
    const action = a("Hello");
    expect(isA(action)).toBe(action);
});

test("The action extractor extracts 'undefined' for actions it does not know about", () => {
    // @ts-ignore
    expect(isB(a("Hello"))).toBeUndefined();
});

test("A reducer should return the default state when the state passed in is undefined and the action is unknown", () => {
    const state = reducer(undefined, c());
    expect(state).toBe(initialState);
});

test("A reducer should only run the state changes for the matched action", () => {
    const state = reducer(initialState, a("Hola"));
    expect(state).toEqual({ a: "Hola", b: 7});
});

test("An action listener should not filter out actions it listens for", done => {
    const action$: Observable<ActionA> = of(a("Hola")).pipe(listenFor(isA));
    class MyObserver {
        private count: number = 0;
        // noinspection JSUnusedGlobalSymbols
        error: (err: any) => void = err => {
            fail(`Unexpected error ${err} was thrown.`);
        };
        // noinspection JSUnusedGlobalSymbols
        next: (action: ActionA) => void = action => {
            this.count++;
            expect(action).toEqual(a("Hola"));
        };
        // noinspection JSUnusedGlobalSymbols
        complete: () => void = () => {
            expect(this.count).toEqual(1);
            done();
        };
    }
    action$.subscribe(new MyObserver());
});

test("An action listener should filter out actions it does not listen for", done => {
    // @ts-ignore
    const action$: Observable<ActionB> = of(a("Hola")).pipe(listenFor(isB));
    // noinspection JSUnusedGlobalSymbols
    action$.subscribe({
        next: action => {
            fail(`Unexpected action ${action} was streamed.`)
        },
        complete: () => {
            done();
        },
        error: err => {
            fail(`Unexpected error ${err} was thrown.`)
        }
    });
});